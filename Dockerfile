FROM registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2021dev1-image-builder

# Hacky install of slirp packages
COPY packages/ ./packages/
RUN dpkg -i packages/*.deb
RUN rm -Rvf packages

# Copy prebuilt debos & libslirp-rs into place
COPY binaries/ ./binaries/
RUN cp binaries/debos /usr/bin/
RUN cp binaries/slirp-helper /usr/bin/
RUN rm -Rvf binaries

# Copy in custom UML kernel and modules
COPY kernel/ ./kernel/
RUN mv kernel/linux /usr/bin/
RUN mv kernel/lib /usr/lib/uml
RUN rm -Rvf kernel
